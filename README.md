# TGit

Some scripts to help managing git branches.

- tbranch: Create a branch and check it out
- tparent: Get or set a branch's parent branch
- tpush: Push a branch and mark it as tracked
- trebase: Rebase a branch on top of its parent
- tmerge: Merge a branch
- tmove: Moves a branch from its parent to a new parent
- tlog: Logs the commits between the parent and the HEAD
- tdiff: Diffs the working tree against the parent
- tbase: Gets the merge-base commit for the parent branch
- tfixup: Fixes up the changes in the current file in the last commit that file touched in the current branch.
- tswitch: Switch to the target-branch, save a stash of the current branch and apply the stash of the target branch (if any).

Run `git COMMAND --help` for more information on each command.

## Installing

To install clone this repo and run `install.sh`. This will create symlinks to
the scripts on the `~/bin` directory (make sure to add it to your path if it's
not there). It will also add some aliases.

