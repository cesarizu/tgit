#!/usr/bin/bash -e

cd "$(dirname $0)"

for a in $(ls git-t*); do
  if [ -f ~/bin/"$a" ]; then
    echo "$a already exists on the ~/bin directory"
  else
    echo "Creating symlink for $a on the ~/bin directory"
    ln -s -t ~/bin $(readlink -f $a)
  fi
done

git config --global alias.tb tbranch
git config --global alias.tp tpush
git config --global alias.tpa tparent
git config --global alias.tr trebase
git config --global alias.tm tmerge
git config --global alias.tmv tmove
git config --global alias.tl tlog
git config --global alias.td tdiff
git config --global alias.tf tfixup
git config --global alias.ts tswitch

