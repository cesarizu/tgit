
STORE="$(git rev-parse --show-toplevel | sed s#D:#/d#g)/.git/tgit"
mkdir -p "$STORE"

MAIN="$(\
  (git rev-parse --quiet --verify develop > /dev/null && echo develop)     || \
  (git rev-parse --quiet --verify master  > /dev/null && echo master)      || \
  (git rev-parse --quiet --verify main    > /dev/null && echo main)        || \
  echo main_branch_not_found)"

CURRENT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"

function real_branch() {
  (test "$1" == "" && echo "")                                             || \
  (git rev-parse --quiet --verify "$1" > /dev/null && echo $1)             || \
  (git symbolic-ref --short --quiet "$1")                                  || \
  (echo $1_branch_not_found)
}

